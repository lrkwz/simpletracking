Fill the order tracking number with "order number" followed by "order date" in YYYYMMDD format.
Implemented with a magento observer hooked on sales_order_shipment_save_before event.

Thanks to [magento save shipment information tracking number carrier code programatically](http://ka.lpe.sh/2012/01/08/magento-save-shipment-information-tracking-number-carrier-code-programatically/)

Contributing
============
Clone the repo as usual, change what you wish and test it in your magento. I suggest you [modgit](http://www.bubblecode.net/en/2012/02/06/install-magento-modules-with-modgit/) as a usefull tool to maintain multiple git based packages in a single magento instance.

Install
=======
Clone the repo and tar all the files, the resulting package can be untarres in magento root or installed via magento connect package manager.\:w


