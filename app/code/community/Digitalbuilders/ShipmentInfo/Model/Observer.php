<?php
class Digitalbuilders_ShipmentInfo_Model_Observer
{
	public function salesOrderShipmentSaveBefore($observer)
	{
		$shipment = $observer->getShipment();
		$order = $shipment->getOrder();
		$last4 = $order->getIncrementId() . $order->getCreatedAtFormatedFree('yyyyMMdd');

		$track = Mage::getModel('sales/order_shipment_track')
			->setNumber($last4) //tracking number / awb number
			->setCarrierCode('custom') //carrier code
			->setTitle('DHL Italia'); //carrier title
		$shipment->addTrack($track);
	}
}
?>
